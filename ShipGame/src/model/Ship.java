package model;

import java.util.Arrays;

public class Ship {
    private int id;
    private boolean isSunk;
    private boolean[] shipParts; // T nietrafiona F trafiona

    public Ship(int shipSize) {
        isSunk = false;
        id = 1;
        shipParts = new boolean[shipSize];
        Arrays.fill(shipParts, true);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isSunk() {

        return isSunk;
    }

    public void setSunk(boolean sunk) {
        isSunk = sunk;
    }

    public boolean[] getShipParts() {
        boolean tmp = false;
        for (int i = 0; i < shipParts.length; i++) {
            if (shipParts[i])
                tmp = false;
            else
                tmp = true;
        }

        if (tmp) {
            isSunk = true;
        }
        return shipParts;
    }

    public void setShipParts(boolean[] shipParts) {

        this.shipParts = shipParts;
    }
}
