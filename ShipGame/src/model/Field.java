package model;

public class Field {
    private Ship ship;
    private int idShipPart;
    private boolean wasMissed;

    public Field(){
        wasMissed = false;
    }

    public boolean isWasMissed() {
        return wasMissed;
    }

    public void setWasMissed(boolean wasMissed) {
        this.wasMissed = wasMissed;
    }

    public Ship getShip() {
        return ship;
    }

    public void setShip(Ship ship) {
        this.ship = ship;
    }

    public int getIdShipPart() {
        return idShipPart;
    }

    public void setIdShipPart(int idShipPart) {
        this.idShipPart = idShipPart;
    }
}
