package model;

import java.util.List;

public class Player {

    /*
        Pomysł -- By zrorbić autonumerowanie okrętów, w klasie player dodać metode ktora bedzie zwracać liczbe dodanych wczesniej statków +1. Dzieki temu uzyskamy dostępny
        kolejny indentyfikator dla nastepnego statku.
     */

    public static boolean round = true; // 1 gracz zaczyna dlatego true.

    private int id;
    private List<Ship> ships;
    private Field[][] board;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<Ship> getShips() {
        return ships;
    }

    public void setShips(List<Ship> ships) {
        this.ships = ships;
    }

    public boolean isRound() {
        return round;
    }

    public void setRound(boolean round) {
        this.round = round;
    }

    public Field[][] getBoard() {
        return board;
    }

    public void setBoard(Field[][] board) {
        this.board = board;
    }
}
