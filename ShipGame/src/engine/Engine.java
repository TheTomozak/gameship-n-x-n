package engine;

import model.Field;
import model.Player;

public class Engine {
    private static boolean isGameOver;
    private static boolean whoWins; // true - Player1, false - Player2

    private Player player1;
    private Player player2;

    private int mapSize;
    private int numberOfFreeFieldsToPutShips;

    public Engine() {
        this.player1 = new Player();
        this.player2 = new Player();

        setIsGameOver(false);
    }

    public void createBoards(int mapSize){
        Field[][] board = new Field[mapSize][mapSize];
        Field[][] board2 = new Field[mapSize][mapSize];

        for(int i=0;i<board.length;i++){
            for(int j=0;j<board[i].length;j++){
                board[i][j] = new Field();
            }
        }

        for(int i=0;i<board2.length;i++){
            for(int j=0;j<board2[i].length;j++){
                board2[i][j] = new Field();
            }
        }

        player1.setBoard(board);
        player2.setBoard(board2);

        this.mapSize = mapSize;
    }

    public void generateNumberOfFreeFieldsToPutShips(){
        this.numberOfFreeFieldsToPutShips = ((mapSize*mapSize)*3)/10;
    }

    public static boolean isIsGameOver() {
        return isGameOver;
    }

    public static void setIsGameOver(boolean isGameOver) {
        Engine.isGameOver = isGameOver;
    }

    public static boolean isWhoWins() {
        return whoWins;
    }

    public static void setWhoWins(boolean whoWins) {
        Engine.whoWins = whoWins;
    }

    public Player getPlayer1() {
        return player1;
    }

    public Player getPlayer2() {
        return player2;
    }

    public int getNumberOfFreeFieldsToPutShips() {
        return numberOfFreeFieldsToPutShips;
    }
}
