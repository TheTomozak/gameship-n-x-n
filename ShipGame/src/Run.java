import engine.Engine;
import model.Field;
import model.Player;
import model.Ship;

import java.util.Scanner;
import java.util.regex.Pattern;

public class Run {

    public static void main(String[] argv) {
        Engine engine = new Engine();

        // Getting map size from user.
        engine.createBoards(
                gettingMapSizeFromUser(new Scanner(System.in))
        );

        // Calculate fields where player need to put ships.
        engine.generateNumberOfFreeFieldsToPutShips();

        // Ship arragment by User.
        System.out.println("PLayer1: ");
        shipArragmentByUser(engine, engine.getPlayer1(), new Scanner(System.in));

        System.out.println("PLayer2: ");
        shipArragmentByUser(engine, engine.getPlayer2(), new Scanner(System.in));

        while(!Engine.isIsGameOver()){
            while(Player.round){
                System.out.println("Now is player 1: ");
                shotingByPlayer(engine, engine.getPlayer2(), new Scanner(System.in));
            }
            while(!Player.round && !Engine.isIsGameOver()){
                System.out.println("Now is player 2: ");
                shotingByPlayer(engine, engine.getPlayer1(), new Scanner(System.in));
            }

            if(!Engine.isIsGameOver()){
                System.out.println("YOU WIN");
            }
        }
    }

    public static void shotingByPlayer(Engine engine, Player player, Scanner scanner){
        drawingBoard(player, "~ ");

        boolean isDataOk = false;
        int x = 0;
        int y = 0;
        while(!isDataOk){
            String dataFromUser = enteringDataAndValidation("\\d*-\\d*", scanner);
            String[] dataFromUserSplited = dataFromUser.split("-");
             x = Integer.parseInt(dataFromUserSplited[0]);
             y = Integer.parseInt(dataFromUserSplited[1]);
            if(x >= 0
                    && x <= player.getBoard().length-1
                    && y >= 0
                    && y <= player.getBoard().length-1){
                isDataOk = true;
            }
        }
        if(player.getBoard()[y][x].getShip() == null) {
            player.getBoard()[y][x].setWasMissed(true);
            Player.round = !Player.round;
        }
        else{
            int index = player.getBoard()[y][x].getIdShipPart();
            player.getBoard()[y][x].getShip().getShipParts()[index] = false;
        }
    }

    public static int gettingMapSizeFromUser(Scanner scanner) {
        boolean isSizeBiggerThanEight = false;
        int mapSize = 0;

        while (!isSizeBiggerThanEight) {
            mapSize = Integer.parseInt(
                    enteringDataAndValidation("\\d*", scanner)
            );
            if (mapSize > 7)
                isSizeBiggerThanEight = true;
        }
        return mapSize;
    }

    public static void drawingBoard(Player player, String dependentStringAimedOnShip) {
        Field[][] board = player.getBoard();

        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {

                if (board[i][j].getShip() == null) {
                    if (board[i][j].isWasMissed())
                        System.out.print("M ");
                    else
                        System.out.print("~ ");
                } else if (board[i][j].getShip() != null) {
                    if (board[i][j].getShip().isSunk())
                        System.out.print("S ");
                    else {
                        if (board[i][j].getShip().getShipParts()
                                [board[i][j].getIdShipPart()]
                        ) {
                            System.out.print(dependentStringAimedOnShip);
                        } else
                            System.out.print("X ");
                    }
                }
            }
            System.out.println();
        }
    }

    public static void shipArragmentByUser(Engine engine, Player player, Scanner scanner) {
        Field[][] board = player.getBoard();
        int tmp = engine.getNumberOfFreeFieldsToPutShips();


        boolean isTextOK = false;
        String textFromUser = "";
        String[] textFromUserSplited;
        while( tmp > 0){
            System.out.println("You need to use " + tmp + " fields.");

            drawingBoard(player, "X ");

            while (!isTextOK){
                textFromUser = enteringDataAndValidation("\\d*-\\d*-\\d*-[pd]", scanner);
                textFromUserSplited = textFromUser.split("-");

                int x = Integer.parseInt(textFromUserSplited[0]);
                int y = Integer.parseInt(textFromUserSplited[1]);

                try {
                    if (textFromUserSplited[3].equals("p")) {
                        for (int i = x; i < x + Integer.parseInt(textFromUserSplited[2]); i++) {
                            if (board[y][i].getShip() != null)
                                break;
                            else
                                isTextOK = true;
                        }
                    } else {
                        for (int i = y; i < y + Integer.parseInt(textFromUserSplited[2]); i++) {
                            if (board[i][x].getShip() != null)
                                break;
                            else
                                isTextOK = true;
                        }
                    }
                }catch (ArrayIndexOutOfBoundsException e){
                    System.out.println("Out of board..");
                    isTextOK = false;
                }
            }
            isTextOK = false;
            textFromUserSplited = textFromUser.split("-");
            Ship ship = new Ship(Integer.parseInt(textFromUserSplited[2]));

            int x = Integer.parseInt(textFromUserSplited[0]);
            int y = Integer.parseInt(textFromUserSplited[1]);

            switch (textFromUserSplited[3]) {
                case "p":
                    for (int i = x, count = 0; i < x + Integer.parseInt(textFromUserSplited[2]); i++, count++) {
                        board[y][i].setShip(ship);
                        board[y][i].setIdShipPart(count);
                    }
                    break;

                case "d":
                    for (int i = y, count = 0; i < y + Integer.parseInt(textFromUserSplited[2]); i++, count++) {
                        board[i][x].setShip(ship);
                        board[i][x].setIdShipPart(count);
                    }
                    break;
            }
            tmp = tmp - Integer.parseInt(textFromUserSplited[2]);
            player.setBoard(board);


        }

//        while (!isTextOK){
//            textFromUser = enteringDataAndValidation("\\d*-\\d*-\\d*-[pd]", scanner);
//            textFromUserSplited = textFromUser.split("-");
//
//            int x = Integer.parseInt(textFromUserSplited[0]);
//            int y = Integer.parseInt(textFromUserSplited[1]);
//
//            try {
//                if (textFromUserSplited[3].equals("p")) {
//                    for (int i = x; i < x + Integer.parseInt(textFromUserSplited[2]); i++) {
//                        if (board[y][i].getShip() != null)
//                            break;
//                        else
//                            isTextOK = true;
//                    }
//                } else {
//                    for (int i = y; i < y + Integer.parseInt(textFromUserSplited[2]); i++) {
//                        if (board[i][x].getShip() != null)
//                            break;
//                        else
//                            isTextOK = true;
//                    }
//                }
//            }catch (ArrayIndexOutOfBoundsException e){
//                System.out.println("Out of board..");
//                isTextOK = false;
//            }
//        }
//        textFromUserSplited = textFromUser.split("-");
//        Ship ship = new Ship(Integer.parseInt(textFromUserSplited[2]));
//
//        int x = Integer.parseInt(textFromUserSplited[0]);
//        int y = Integer.parseInt(textFromUserSplited[1]);
//
//        switch (textFromUserSplited[3]) {
//            case "p":
//                for (int i = x, count = 0; i < x + Integer.parseInt(textFromUserSplited[2]); i++, count++) {
//                    board[y][i].setShip(ship);
//                    board[y][i].setIdShipPart(count);
//                }
//                break;
//
//            case "d":
//                for (int i = y, count = 0; i < y + Integer.parseInt(textFromUserSplited[2]); i++, count++) {
//                    board[i][x].setShip(ship);
//                    board[i][x].setIdShipPart(count);
//                }
//                break;
//        }
//        player.setBoard(board);
    }

    public static String enteringDataAndValidation(String pattern, Scanner scanner) {
        boolean didUserTypedCorrectData = false;
        Pattern patternObject = Pattern.compile(pattern);
        String textFromUser = "";

        while (!didUserTypedCorrectData) {
            System.out.print("Please type required data: ");
            textFromUser = scanner.nextLine();

            if (patternObject
                    .matcher(textFromUser)
                    .matches()
                    && !textFromUser.equals(""))
                didUserTypedCorrectData = true;
            else
                System.out.println("Wrong type of data, try again..");

            scanner = new Scanner(System.in);
        }
        return textFromUser;
    }
}
